package org.mivoligo.nightclock


import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import org.mivoligo.nightclock.databinding.FragmentOwnColorBinding


class OwnColorFragment : Fragment() {

    private var _binding: FragmentOwnColorBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        _binding = FragmentOwnColorBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onStart() {
        super.onStart()

        val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        val isDayMode = prefs[getString(R.string.pref_day_mode_key), false]
        val bundleColor = arguments?.getString("selected_color")
        val bundleIsBackColor = arguments?.getBoolean("isBackColor")
        val color = Color.parseColor(bundleColor)
        var red = Color.red(color)
        var green = Color.green(color)
        var blue = Color.blue(color)
        binding.tvRed.text = red.toString()
        binding.tvGreen.text = green.toString()
        binding.tvBlue.text = blue.toString()
        binding.sbRed.progress = red
        binding.sbGreen.progress = green
        binding.sbBlue.progress = blue

        fun setColor(red: Int, green: Int, blue: Int): String {
            val colorInt = Color.rgb(red, green, blue)
            return String.format("#%06X", (0xFFFFFF and colorInt))
        }

        fun setForegroundColorPreview(color: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.day_mode_fore_color_preview_key) to color) }
            } else {
                prefs.edit { put(getString(R.string.night_mode_fore_color_preview_key) to color) }
            }
        }

        fun setBackgroundColorPreview(color: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.day_mode_back_color_preview_key) to color) }
            } else {
                prefs.edit { put(getString(R.string.night_mode_back_color_preview_key) to color) }
            }
        }

        binding.sbRed.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar, progress: Int, p2: Boolean) {
                red = progress
                binding.tvRed.text = red.toString()
                if (bundleIsBackColor!!) {
                    setBackgroundColorPreview(setColor(red, green, blue))
                } else {
                    setForegroundColorPreview(setColor(red, green, blue))
                }

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })

        binding.sbGreen.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar, progress: Int, p2: Boolean) {
                green = progress
                binding.tvGreen.text = green.toString()
                if (bundleIsBackColor!!) {
                    setBackgroundColorPreview(setColor(red, green, blue))
                } else {
                    setForegroundColorPreview(setColor(red, green, blue))
                }

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })

        binding.sbBlue.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar, progress: Int, p2: Boolean) {
                blue = progress
                binding.tvBlue.text = blue.toString()
                if (bundleIsBackColor!!) {
                    setBackgroundColorPreview(setColor(red, green, blue))
                } else {
                    setForegroundColorPreview(setColor(red, green, blue))
                }

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
